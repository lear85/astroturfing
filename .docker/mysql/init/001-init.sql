# purge all databases
DROP DATABASE IF EXISTS `mtg_finder`;
# create databases
CREATE DATABASE IF NOT EXISTS `mtg_finder`;
