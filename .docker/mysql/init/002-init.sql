# purge all databases
DROP DATABASE IF EXISTS `mtg_finder_test`;
# create databases
CREATE DATABASE IF NOT EXISTS `mtg_finder_test`;
