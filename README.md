Astroturfing
============

What is astroturfing ?
----------------------

Astroturfing is the practice of masking the sponsors of a message or organization (e.g., political, advertising, religious or public relations)
to make it appear as though it originates from and is supported by grassroots participants.
It is a practice intended to give the statements or organizations credibility by withholding information about the source's financial connection.
The term Astroturfing is derived from AstroTurf, a brand of synthetic carpeting designed to resemble natural grass, as a play
on the word "grassroots". The implication behind the use of the term is that instead of a "true" or "natural" grassroots 
effort behind the activity in question (such as a survey), there is a "fake" or "artificial" appearance of support.

The practice is often used in political campaigns to create the illusion of a popular movement supporting a candidate or policy,
or to create the impression that the candidate has momentum.

What this script does ?
-----------------------

This script provide basic tool to send fake form submission to a website.
It can be used to test the security of a website against Astroturfing... Or to expose vulnerabilities to the website owner.
Trial works that way: On a given survey, if you can't find with easily if a vote is fake or not, then the website is vulnerable to Astroturfing.

How does it works ?
-------------------

The script is based on the fact that the website is using a form to submit the vote and is protected against CSRF (Cross Site Request Forgery).
We are using a basic html crawler to fetch the form and its fields, then replace values with our own fake data.

How to use it ?
---------------

You need to setup docker and make on your machine.
Everything is setup to run on a docker container within a linux computer, or directly on a linux server.


```bash
git clone git@gitlab.com:lear85/astroturfing.git
cd astroturfing
```

Create a `.env.local` file with

    ###> docker-compose ###
    GITHUB_TOKEN=[You github token]
    GIT_EMAIL="Your email"
    GIT_NAME="You name"
    ###< docker-compose ###

```bash
make build
make start
```

You can enter docker ssh with the following command:

```bash
make ssh
```

Then you need to install the dependencies:

```bash
composer install
```

Then you need to setup the database and the schema:

```bash
php bin/console doctrine:database:create
```

Now you are ready to update the [GenerateVoteCommand.php](src%2FCommand%2FGenerateVoteCommand.php) file to match your needs.
Then run the script with the following command:

```bash
php app/console app:generate-vote
```

The script will be save in database.
Double check the data before running the script using phpmyadmin on you local machine.

Then run the script with the following command:

```bash
php bin/console app:astroturfing
```

The script will send the fake votes to the website, one vote every 1 to 30 seconds (randomized)

Exemple of usage:
-----------------

Demo provide a way to fool a survey about a local political question.
For the sake of counter measure effectiveness, this tool has been stripped of all the complex way we can setup to "make it like real humains".

![Robot validating a captcha](doc/robot.gif)

Why providing this script ?
---------------------------

Astroturfing is a major threat to democracy. It is used by political parties to influence the public opinion.
It is also used by companies to influence the public opinion about their products.
The best way to avoid Astroturfing is to expose it.
This script is a tool to expose Astroturfing as an easy solution to gain support against unpopular politics.

Never trust an online survey, always double check the results.
Never trust a political assumption, always double check the facts.
You want to know your electorate opinion ? Organise a real vote.
Otherwise, it's propaganda.
