<?php

namespace App\Entity;

use App\Repository\VoteRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: VoteRepository::class)]
class Vote
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 4)]
    private ?string $school = null;

    /** @var string|null */
    #[ORM\Column(length: 1, nullable: true)]
    private ?string $uniformA = null;

    /** @var string|null */
    #[ORM\Column(length: 1, nullable: true)]
    private ?string $uniformB = null;

    /** @var string|null */
    #[ORM\Column(length: 1, nullable: true)]
    private ?string $flagA = null;

    /** @var string|null */
    #[ORM\Column(length: 1, nullable: true)]
    private ?string $flagB = null;

    /** @var string|null */
    #[ORM\Column(length: 1, nullable: true)]
    private ?string $helpA = null;

    /** @var string|null */
    #[ORM\Column(length: 1, nullable: true)]
    private ?string $helpB = null;

    #[ORM\Column(nullable: true)]
    private ?\DateTimeImmutable $sentAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSchool(): ?string
    {
        return $this->school;
    }

    public function setSchool(string $school): static
    {
        $this->school = $school;

        return $this;
    }

    public function getUniformA(): ?string
    {
        return $this->uniformA;
    }

    public function setUniformA(?string $uniformA): self
    {
        $this->uniformA = $uniformA;

        return $this;
    }

    public function getUniformB(): ?string
    {
        return $this->uniformB;
    }

    public function setUniformB(?string $uniformB): self
    {
        $this->uniformB = $uniformB;

        return $this;
    }

    public function getFlagA(): ?string
    {
        return $this->flagA;
    }

    public function setFlagA(?string $flagA): self
    {
        $this->flagA = $flagA;

        return $this;
    }

    public function getFlagB(): ?string
    {
        return $this->flagB;
    }

    public function setFlagB(?string $flagB): self
    {
        $this->flagB = $flagB;

        return $this;
    }

    public function getHelpA(): ?string
    {
        return $this->helpA;
    }

    public function setHelpA(?string $helpA): self
    {
        $this->helpA = $helpA;

        return $this;
    }

    public function getHelpB(): ?string
    {
        return $this->helpB;
    }

    public function setHelpB(?string $helpB): self
    {
        $this->helpB = $helpB;

        return $this;
    }

    public function getSentAt(): ?\DateTimeImmutable
    {
        return $this->sentAt;
    }

    public function setSentAt(?\DateTimeImmutable $sentAt): static
    {
        $this->sentAt = $sentAt;

        return $this;
    }
}
