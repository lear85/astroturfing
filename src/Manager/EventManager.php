<?php
/**
 * @licence Proprietary
 */
namespace App\Manager;

use App\Entity\Event;
use App\Entity\EventRecurring;
use App\Entity\EventRecurringPeriodicityEnum;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class EventManager
 */
class EventManager
{
    public function __construct(
        private EntityManagerInterface $manager,
    ) { }

    public function create(Event $event = null): Event
    {
        $event = $event ?? new Event();
        $event->setCode($this->createUnusedCode());

        return $event;
    }

    public function getNextEventDate(EventRecurring $eventRecurring): \DateTimeInterface
    {
        // Get last event
        $lastEvent = $eventRecurring->getEvents()->last();
        // If there is no last event, throw exception. Should never happen
        if (!$lastEvent) {
            throw new \Exception(sprintf('No last event found on event recurring %d', $eventRecurring->getId()));
        }

        $startAt = clone $lastEvent->getStartAt();
        // set first day of the week to apply day of week on it
        $startAt = $startAt->modify('Last Monday');
        // Only add if not monday
        if ($eventRecurring->getDayOfWeek()) {
            $startAt = $startAt->modify(sprintf('+%d days', $eventRecurring->getDayOfWeek()));
        }
        // Add interval based on periodicity field
        switch ($eventRecurring->getPeriodicity()) {
            case EventRecurringPeriodicityEnum::Weekly:
                $startAt = $startAt->modify('+1 week');
                break;
            case EventRecurringPeriodicityEnum::TwoWeekly:
                $startAt = $startAt->modify('+2 week');
                break;
            case EventRecurringPeriodicityEnum::Monthly:
                $startAt = $startAt->modify('+1 month');
                break;
        }

        return $startAt;
    }

    public function createRecurring(Event $event)
    {
        // Create if not already embedded in event
        $eventRecurring = $event->getEventRecurring() ?: new EventRecurring();
        $eventRecurring->setTemplate($event);
        $eventRecurring->setCreatedBy($event->getCreatedBy());
        $eventRecurring->setDayOfWeek($event->getStartAt()->format('N') - 1);
        $eventRecurring->setNextAt($this->getNextEventDate($eventRecurring));

        return $eventRecurring;
    }

    /**
     * Generate an event based on a recurring event
     * If the time limit is reached, the event is not generated
     *
     * @param EventRecurring $eventRecurring
     * @param int $limit
     *
     * @return Event|null
     * @throws \Exception
     */
    public function generate(EventRecurring $eventRecurring, \DateTimeInterface $limit): ?Event
    {
        // If the next event is later than the limit, don't generate it.
        if ($eventRecurring->getNextAt() > $limit) {
            return null;
        }

        $template = $eventRecurring->getTemplate();
        $event = clone $template;
        $this->create($event);
        $startAt = clone $eventRecurring->getNextAt();
        $startAt = $startAt->setTime(
            $template->getStartAt()->format('H'),
            $template->getStartAt()->format('i')
        );
        $event->setStartAt(clone $startAt);
        // Get template event duration
        $duration = $template->getStartAt()->diff($template->getEndAt());
        // Add duration to start date to get end date
        $event->setEndAt((clone $startAt)->add($duration));

        $eventRecurring->addEvent($event);
        // Schedule next event generation date
        $eventRecurring->setNextAt($this->getNextEventDate($eventRecurring));
        // Replace template with new event to keep track of the potential changes
        $eventRecurring->setTemplate($event);

        return $event;
    }

    public function createUnusedCode(): string
    {
        $code = $this->generateCode();
        $event = $this->manager->getRepository(Event::class)->findOneBy(['code' => $code]);
        // Retry if the code is already used
        if ($event) {
            return $this->createUnusedCode();
        }

        return $code;
    }

    /**
     * Create 8 alphanumeric characters long string.
     * The code can't contain the letters O and I to avoid confusion with 0 and 1.
     *
     * @return string
     */
    public function generateCode(): string
    {
        $code = '';
        $chars = 'ABCDEFGHJKLMNPQRSTUVWXYZ23456789';

        for ($i = 0; $i < 6; $i++) {
            $code .= $chars[rand(0, strlen($chars) - 1)];
        }

        // Add a dash every 3 characters to ensure readability
        // and avoid confusion with compagnon codes
        return 'MF-' . implode('-', str_split($code, 3));
    }
}
