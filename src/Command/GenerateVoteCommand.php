<?php
/**
 * @licence Proprietary
 */
namespace App\Command;

use App\Entity\Vote;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerateVoteCommand
 *
 * Values are based on a limesurvey survey with 500 votes.
 * 7 fields in the form :
 * - One for school ID
 * - 2 for uniform (1 per parent)
 * - 2 for flag and national anthem (1 per parent)
 * - 2 for homework assistance by volunteers (1 per parent)
 */
#[AsCommand(
    name: 'app:generate-vote',
    description: 'Generate X votes.',
)]
class GenerateVoteCommand extends Command
{
    protected $schools = [
        'AO01' => 220,
        'AO02' => 154,
        'AO03' => 33,
        'AO05' => 49,
    ];

    public function __construct(
        protected EntityManagerInterface $manager
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp('Generate X/2 votes form.')
            ->addArgument('limit', InputArgument::OPTIONAL, 'Limit of votes to generate', 500)
            ->addOption('truncate', 't', InputOption::VALUE_OPTIONAL, 'Truncate table first', true)
            ->addOption('uniformRate', '1', InputOption::VALUE_OPTIONAL, 'Number of N value for uniform', 80)
            ->addOption('flagRate', '2', InputOption::VALUE_OPTIONAL, 'Number of N value for flag', 85)
            ->addOption('helpRate', '3', InputOption::VALUE_OPTIONAL, 'Number of N value for help', 35)
            ->addOption('noise', '4', InputOption::VALUE_OPTIONAL, 'Number of empty value in non N range', 10)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Generate X votes');

        if ($input->getOption('truncate')) {
            $output->writeln('Truncate table first');

            $voteRepository = $this->manager->getRepository(Vote::class);
            $voteRepository->truncate();
        }

        $limit = $input->getArgument('limit');
        $uniformRate = $input->getOption('uniformRate');
        $flagRate = $input->getOption('flagRate');
        $helpRate = $input->getOption('helpRate');
        // Noise is null vote, rest is Yes
        $noise = $input->getOption('noise');

        $output->writeln(sprintf(
            'Generate %d votes with %d%% uniform No, %d%% flag No, %d%% help No and %d%% percent of blank value on what\'s left',
            $limit,
            $uniformRate,
            $flagRate,
            $helpRate,
            $noise
        ));

        // Generate array with $limit entries, filled with $limit*100/$rate
        $uniform = array_fill(0, $limit*$uniformRate/100, 'N');
        $noiseRange = count($uniform) * $noise / 100;
        $uniform = array_merge($uniform, array_fill(count($uniform), round($noiseRange), null));
        if ($limit - count($uniform) < 0) {
            throw new \Exception('Off limit');
        }
        $uniform = array_merge($uniform, array_fill(count($uniform), $limit - count($uniform), 'Y'));

        // Generate array with $limit entries, filled with $limit*100/$rate
        $flag = array_fill(0, $limit*$flagRate/100, 'N');
        $noiseRange = count($flag) * $noise / 100;
        $flag = array_merge($flag, array_fill(count($flag), round($noiseRange), null));
        if ($limit - count($flag) < 0) {
            throw new \Exception('Off limit');
        }
        $flag = array_merge($flag, array_fill(count($flag), $limit - count($flag), 'Y'));

        // Generate array $limit entries, filled with $limit*100/$rate
        $help = array_fill(0, $limit*$helpRate/100, 'N');
        $noiseRange = count($help) * $noise / 100;
        $help = array_merge($help, array_fill(count($help), round($noiseRange), null));
        if ($limit - count($help) < 0) {
            throw new \Exception('Off limit');
        }
        $help = array_merge($help, array_fill(count($help), $limit - count($help), 'Y'));

        // Create school array.
        $schools = [];
        do {
            foreach ($this->schools as $id => $count) {
                $schools = array_merge($schools, array_fill(count($schools), $count, $id));
            }
        } while (count($schools) < ($limit / 2));

        // Shuffle all arrays
        shuffle($schools);
        shuffle($uniform);
        shuffle($flag);
        shuffle($help);

        // Create votes
        for ($i = 0; $i < $limit / 2; $i++) {
            $vote = new Vote();
            $vote->setSchool($schools[$i]);
            $vote->setUniformA($uniform[$i]);
            $vote->setUniformB($uniform[$limit / 2 + $i]);
            $vote->setFlagA($flag[$i]);
            $vote->setFlagB($flag[$limit / 2 + $i]);
            $vote->setHelpA($help[$i]);
            $vote->setHelpB($help[$limit / 2 + $i]);
            $this->manager->persist($vote);
        }

        $this->manager->flush();

        return 0;
    }
}
