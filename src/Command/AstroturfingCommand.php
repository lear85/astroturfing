<?php
/**
 * @licence Proprietary
 */
namespace App\Command;

use App\Entity\Vote;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Class AstroturfingCommand
 */
#[AsCommand(
    name: 'app:astroturfing',
    description: 'Send all votes.',
)]
class AstroturfingCommand extends Command
{
    public function __construct(
        protected EntityManagerInterface $manager
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            // the command help shown when running the command with the "--help" option
            ->setHelp('Send all votes.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // Get all votes
        $votes = $this->manager->getRepository(Vote::class)->findBy([
            'sentAt' => null,
        ]);

        // Create map from votes
        foreach ($votes as $row => $vote) {
            $output->writeln(sprintf(
                'call %d : School %s | Uniforme M:%s|F:%s | Drapeau M:%s|F:%s | Aide M:%s|F:%s',
                $vote->getId(),
                $vote->getSchool(),
                $vote->getUniformA(),
                $vote->getUniformB(),
                $vote->getFlagA(),
                $vote->getFlagB(),
                $vote->getHelpA(),
                $vote->getHelpB()
            ));
            if ($this->call([
                // name of the field => value
                'start_time' => time() - rand(30, 120),
                '578536X1X1' => $vote->getSchool(),
                '578536X2X6' => (string) $vote->getUniformA(),
                '578536X2X18' => (string) $vote->getUniformB(),
                '578536X3X10' => (string) $vote->getFlagA(),
                '578536X3X19' => (string) $vote->getFlagB(),
                '578536X4X14' => (string) $vote->getFlagA(),
                '578536X4X20' => (string) $vote->getFlagB(),
            ])) {
                $vote->setSentAt(new \DateTimeImmutable());
                $this->manager->persist($vote);
                $this->manager->flush();

                $output->writeln('<info>Success</info>');
            } else {
                $output->writeln('<error>Failed</error>');
            }

            sleep(rand(1, 30));
        }

        return 0;
    }

    protected function call(array $map): bool
    {
        $browser = new HttpBrowser(HttpClient::create());
        $crawler = $browser->request('GET', 'https://enquetes.talmontsainthilaire.fr/index.php/238819');
        $form = $crawler->selectButton('Envoyer')->form();
        $form->setValues($map);
        // Add a wait function here to simulate user browsing the survey
        sleep(rand(15, 120));
        $crawler = $browser->submit($form);

        // Check if the form has been submitted by looking for the success message
        return strpos($crawler->filter('body')->first()->text(), 'avoir particip') !== false;
    }
}
