<?php

namespace App\Repository;

use App\Entity\Vote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Vote>
 *
 * @method Vote|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vote|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vote[]    findAll()
 * @method Vote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vote::class);
    }

    public function truncate()
    {
        $sql = <<<SQL
SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE TABLE vote;
SET FOREIGN_KEY_CHECKS = 1;
SQL;
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);

        return $stmt->executeStatement();
    }
}
